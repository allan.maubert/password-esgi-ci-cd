# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
from pip._vendor import requests


def ask_password():
    return input("Entrez votre mot de passe : ")


def worst_passwords():
    res = requests.get(
        'https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Common-Credentials/500-worst-passwords.txt')
    return res.text


def most_commons_passwords():
    res = requests.get(
        'https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Common-Credentials/10k-most-common.txt')
    return res.text


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    password_user = ask_password()
    print(f"password : {password_user}")

    worst_passwords = worst_passwords()
    if password_user in worst_passwords:
        print(f"password '{password_user}'  is really bad")
        exit(0)

    commons_passwords = most_commons_passwords()
    if password_user in commons_passwords:
        print(f"password '{password_user}'  is common, change it please .")
        exit(0)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
